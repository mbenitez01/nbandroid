/*
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package org.netbeans.modules.android.lint.analyzer;

import com.android.tools.lint.checks.BuiltinIssueRegistry;
import com.android.tools.lint.detector.api.Issue;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import javax.swing.JComponent;
import org.netbeans.api.fileinfo.NonRecursiveFolder;
import org.netbeans.modules.analysis.spi.Analyzer;
import org.netbeans.modules.android.lint.RunLint;
import org.netbeans.modules.android.lint.Utilities;
import org.netbeans.spi.editor.hints.ErrorDescription;
import org.openide.filesystems.FileObject;
import org.openide.util.NbBundle.Messages;
import org.openide.util.Union2;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author lahoda@gmail.com
 */
public class AnalyzerImpl implements Analyzer {

    private final Context context;
    private final AtomicReference<RunLint> currentWorker = new AtomicReference<RunLint>();

    public AnalyzerImpl(Context context) {
        this.context = context;
    }

    @Override
    public Iterable<? extends ErrorDescription> analyze() {
        RunLint lint = new RunLint();

        currentWorker.set(lint);

        try {
            List<Union2<FileObject, NonRecursiveFolder>> toAnalyze = new ArrayList<Union2<FileObject, NonRecursiveFolder>>();
            for (FileObject f : context.getScope().getFiles()) {
                toAnalyze.add(Union2.<FileObject, NonRecursiveFolder>createFirst(f));
            }
            for (NonRecursiveFolder f : context.getScope().getFolders()) {
                toAnalyze.add(Union2.<FileObject, NonRecursiveFolder>createSecond(f));
            }
            for (FileObject f : context.getScope().getSourceRoots()) {
                toAnalyze.add(Union2.<FileObject, NonRecursiveFolder>createFirst(f));
            }
            //XXX: should add manifest and proguard file
            return lint.analyze(toAnalyze);
        } finally {
            currentWorker.set(null);
        }
    }

    @Override
    public boolean cancel() {
        RunLint l = currentWorker.get();

        if (l != null) {
            l.cancel();
        }

        return true;
    }

    @ServiceProvider(service=AnalyzerFactory.class)
    public static final class AnalyzerFactoryImpl extends AnalyzerFactory {

        @Messages("DN_AndroidLint=Android Lint")
        public AnalyzerFactoryImpl() {
            super("android-lint", Bundle.DN_AndroidLint(), "com/android/tools/lint/lint-run.png");
        }

        @Override
        public Iterable<? extends WarningDescription> getWarnings() {
            List<WarningDescription> result = new ArrayList<WarningDescription>();

            for (Issue i : new BuiltinIssueRegistry().getIssues()) {
                result.add(WarningDescription.create(i.getId(), Utilities.displayName(i), i.getCategory().getName(), i.getCategory().getName()));
            }

            return result;
        }

        @Override
        public <D, C extends JComponent> CustomizerProvider<D, C> getCustomizerProvider() {
            return null;//for now...
        }

        @Override
        public Analyzer createAnalyzer(Context context) {
            return new AnalyzerImpl(context);
        }

    }
}
