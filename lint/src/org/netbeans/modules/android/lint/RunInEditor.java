/*
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package org.netbeans.modules.android.lint;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import org.netbeans.api.editor.mimelookup.MimeRegistration;
import org.netbeans.api.fileinfo.NonRecursiveFolder;
import org.netbeans.api.java.source.CancellableTask;
import org.netbeans.api.java.source.CompilationInfo;
import org.netbeans.api.java.source.JavaSource.Phase;
import org.netbeans.api.java.source.JavaSource.Priority;
import org.netbeans.api.java.source.JavaSourceTaskFactory;
import org.netbeans.api.java.source.support.EditorAwareJavaSourceTaskFactory;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.Parser.Result;
import org.netbeans.modules.parsing.spi.ParserResultTask;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.modules.parsing.spi.SchedulerTask;
import org.netbeans.modules.parsing.spi.TaskFactory;
import org.netbeans.spi.editor.hints.ErrorDescription;
import org.netbeans.spi.editor.hints.HintsController;
import org.openide.filesystems.FileObject;
import org.openide.util.Union2;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author lahoda@gmail.com
 */
public class RunInEditor extends ParserResultTask<Parser.Result> implements CancellableTask<CompilationInfo> {

    private final AtomicReference<RunLint> currentWorker = new AtomicReference<RunLint>();
    
    List<ErrorDescription> analyze(final Snapshot snapshot) {
        RunLint lint = new RunLint();

        currentWorker.set(lint);

        try {
            return lint.analyze(Collections.singleton(Union2.<FileObject, NonRecursiveFolder>createFirst(snapshot.getSource().getFileObject())));
        } finally {
            currentWorker.set(null);
        }
    }
        
    @Override
    public void run(final Result t, SchedulerEvent se) {
        doRun(t.getSnapshot());
    }

    private void doRun(Snapshot snapshot) {
        Collection<? extends ErrorDescription> errors = analyze(snapshot);
        
        HintsController.setErrors(snapshot.getSource().getFileObject(), RunInEditor.class.getName(), errors);
    }

    @Override
    public int getPriority() {
        return 1000;
    }

    @Override
    public Class<? extends Scheduler> getSchedulerClass() {
        return Scheduler.EDITOR_SENSITIVE_TASK_SCHEDULER;
    }

    @Override
    public void cancel() {
        RunLint l = currentWorker.get();

        if (l != null) {
            l.cancel();
        }
    }

    @Override
    public void run(CompilationInfo info) throws Exception {
        doRun(info.getSnapshot());
    }

    @MimeRegistration(mimeType="text/xml", service=TaskFactory.class)
    public static final class FactoryImpl extends TaskFactory {

        @Override
        public Collection<? extends SchedulerTask> create(Snapshot snpsht) {
            return Collections.singletonList(new RunInEditor());
        }
        
    }

    @ServiceProvider(service=JavaSourceTaskFactory.class)
    public static final class JavaFactoryImpl extends EditorAwareJavaSourceTaskFactory {
        public JavaFactoryImpl() {
            super(Phase.RESOLVED, Priority.MIN);
        }

        @Override
        protected CancellableTask<CompilationInfo> createTask(FileObject file) {
            return new RunInEditor();
        }

    }
}
