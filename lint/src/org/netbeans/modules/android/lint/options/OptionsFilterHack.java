/*
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package org.netbeans.modules.android.lint.options;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTree;
import javax.swing.tree.TreeModel;
import org.openide.util.Lookup;

/**options.editor does not provide a public API. Workarounding using reflection.
 *
 * @author lahoda@gmail.com
 */
public class OptionsFilterHack {
    
    private static boolean initialized;
    private static ClassLoader mainClassLoader;
    private static Class<?> optionsFilterClass;
    private static Class<?> acceptorClass;
    
    public static synchronized OptionsFilterHack create(Lookup master) {
        if (!initialized) {
            initialized = true;
            try {
                mainClassLoader = Lookup.getDefault().lookup(ClassLoader.class);
                
                if (mainClassLoader == null) return null;
                
                optionsFilterClass = Class.forName("org.netbeans.modules.options.editor.spi.OptionsFilter", false, mainClassLoader);
                acceptorClass = Class.forName("org.netbeans.modules.options.editor.spi.OptionsFilter$Acceptor", false, mainClassLoader);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(OptionsFilterHack.class.getName()).log(Level.FINE, null, ex);
                return null;
            }
        }
        
        if (acceptorClass == null) return null;
        
        Object optionsFilter = master.lookup(optionsFilterClass);
        
        if (optionsFilter == null) return null;
        
        return new OptionsFilterHack(optionsFilter);
    }
    
    private final Object optionsFilter;
    
    public OptionsFilterHack(Object optionsFilter) {
        this.optionsFilter = optionsFilter;
    }
    
    public boolean installFilteringModel(JTree tree, TreeModel baseModel, Acceptor acceptor) {
        try {
            Method m = optionsFilterClass.getDeclaredMethod("installFilteringModel", JTree.class, TreeModel.class, acceptorClass);
            Object optionsAcceptor = Proxy.newProxyInstance(mainClassLoader, new Class[] {acceptorClass}, new InvocationHandlerImpl(acceptor));
            
            m.invoke(optionsFilter, tree, baseModel, optionsAcceptor);
            return true;
        } catch (IllegalAccessException ex) {
            Logger.getLogger(OptionsFilterHack.class.getName()).log(Level.FINE, null, ex);
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(OptionsFilterHack.class.getName()).log(Level.FINE, null, ex);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(OptionsFilterHack.class.getName()).log(Level.FINE, null, ex);
        } catch (NoSuchMethodException ex) {
            Logger.getLogger(OptionsFilterHack.class.getName()).log(Level.FINE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(OptionsFilterHack.class.getName()).log(Level.FINE, null, ex);
        }
        return false;
    }
    
    public interface Acceptor {
        public boolean accept(Object originalTreeNode, String filterText);
    }
    
    private static final class InvocationHandlerImpl implements InvocationHandler {

        private final Acceptor acceptor;

        public InvocationHandlerImpl(Acceptor acceptor) {
            this.acceptor = acceptor;
        }
        
        @Override public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            if ("accept".equals(method.getName())) {
                return acceptor.accept(args[0], (String) args[1]);
            }
            if ("equals".equals(method.getName())) {
                return acceptor.equals(args[0]);
            }
            if ("hashCode".equals(method.getName())) {
                return acceptor.hashCode();
            }
            throw new UnsupportedOperationException();
        }
        
    }
}
