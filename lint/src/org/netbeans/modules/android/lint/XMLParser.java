/*
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package org.netbeans.modules.android.lint;

import java.util.Collection;
import javax.swing.event.ChangeListener;
import org.netbeans.api.editor.mimelookup.MimeRegistration;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Task;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.ParserFactory;
import org.netbeans.modules.parsing.spi.SourceModificationEvent;

/**There must be a parser so that the parser based tasks work.
 *
 * @author lahoda@gmail.com
 */
public class XMLParser extends Parser {

    private Snapshot snapshot;
    @Override
    public void parse(Snapshot snpsht, Task task, SourceModificationEvent sme) throws ParseException {
        this.snapshot = snpsht;
    }

    @Override
    public Result getResult(Task task) throws ParseException {
        return new Result(snapshot) {
            @Override protected void invalidate() { }
        };
    }

    @Override
    public void addChangeListener(ChangeListener cl) { }

    @Override
    public void removeChangeListener(ChangeListener cl) { }

    @Override
    public void cancel() { }

    @MimeRegistration(mimeType="text/xml", service=ParserFactory.class)
    public static final class FactoryImpl extends ParserFactory {
        @Override public Parser createParser(Collection<Snapshot> clctn) {
            return new XMLParser();
        }
    }
    
}
