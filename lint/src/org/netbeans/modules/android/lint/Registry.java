/*
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package org.netbeans.modules.android.lint;

import com.android.tools.lint.checks.BuiltinIssueRegistry;
import com.android.tools.lint.client.api.IssueRegistry;
import com.android.tools.lint.detector.api.Issue;
import com.android.tools.lint.detector.api.Scope;
import java.util.EnumSet;
import java.util.Set;
import java.util.prefs.Preferences;
import org.openide.util.NbPreferences;

/**
 *
 * @author lahoda@gmail.com
 */
public class Registry {
    
    private static final IssueRegistry LINT_REGISTRY = new BuiltinIssueRegistry();
    public static final EnumSet<Scope> EDITOR_SCOPES = EnumSet.of(Scope.JAVA_FILE, Scope.MANIFEST, Scope.PROGUARD_FILE, Scope.RESOURCE_FILE);
    
    public static IssueRegistry getLintRegistry() {
        return LINT_REGISTRY;
    }
    
    public static Preferences getPreferences() {
        return NbPreferences.forModule(Registry.class);
    }
    
    public static void setEnabled(String id, boolean enabled) {
        setEnabled(getPreferences(), id, enabled);
    }

    public static void setEnabled(Preferences prefs, String id, boolean enabled) {
        prefs.putBoolean("issue-" + id, enabled);
    }
    
    public static void setEnabledInEditor(Preferences prefs, String id, boolean enabled) {
        prefs.putBoolean("editor-issue-" + id, enabled);
    }
    
    public static boolean isEnabled(String id) {
        return isEnabled(getPreferences(), id);
    }
    
    public static boolean isEnabled(Preferences prefs, String id) {
        Issue issue = LINT_REGISTRY.getIssue(id);
        
        return prefs.getBoolean("issue-" + id, issue.isEnabledByDefault());
    }
    
    public static boolean isEnabledInEditor(Preferences prefs, String id) {
        Issue issue = LINT_REGISTRY.getIssue(id);
        
        return isEnabled(prefs, id) && prefs.getBoolean("editor-issue-" + id, isSupportedInEditor(id));
    }
    
    public static boolean isSupportedInEditor(String id) {
        Issue issue = LINT_REGISTRY.getIssue(id);
        Set<Scope> s = EnumSet.copyOf(EDITOR_SCOPES);

        s.retainAll(issue.getScope());
        
        return !s.isEmpty();
    }
}
