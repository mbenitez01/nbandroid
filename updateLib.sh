#!/bin/bash

updateLib() {
  group=$1
  artifact=$2
  module=$3
  cp ~/.m2/repository/${group}/${artifact}/${new}/${artifact}-${new}.jar ${module}/release/modules/ext/
  hg remove ${module}/release/modules/ext/${artifact}-${old}.jar
  hg add ${module}/release/modules/ext/${artifact}-${new}.jar
}

updateSdk() {
  old=23.1.3
  new=24.0.0

  updateLib com/android/tools common commonlib
  updateLib com/android/tools/ddms ddmlib ddmlib
  updateLib com/android/tools/build manifest-merger ide_common
  updateLib com/android/tools sdk-common ide_common
  updateLib com/android/tools/layoutlib layoutlib-api layoutlib_api
  updateLib com/android/tools dvlib sdklib
  updateLib com/android/tools sdklib sdklib
  find * -name project.xml -exec sed --in-place "s/${old}/${new}/g" {} \;
  find * -name manifest.mf -exec sed --in-place "s/${old}/${new}/g" {} \;
}

updateGradle() {
  old=0.13.3
  new=1.0.0

  updateLib com/android/tools/build builder-model buildtools
  updateLib com/android/tools/build builder buildtools
  updateLib com/android/tools/build builder-test-api buildtools
  updateLib com/android/tools/build gradle buildtools
  find * -name project.xml -exec sed --in-place "s/${old}/${new}/g" {} \;
  find * -name manifest.mf -exec sed --in-place "s/${old}/${new}/g" {} \;
}

updateSdk
updateGradle

