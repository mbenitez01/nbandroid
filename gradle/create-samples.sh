mkdir ~/tmp/gradle-samples-0.6.1
cp -r tests/* ~/tmp/gradle-samples-0.6.1
cd ~/tmp/gradle-samples-0.6.1
rm build.gradle
rm -rf repo
find * -name build.gradle -exec sed -i  's/maven {.*}/mavenCentral\(\)/g' {} \;
find * -name build.gradle -exec sed -i  's/0.6.1-SNAPSHOT/0.6.+/g' {} \;
zip -r gradle-samples-0.6.1.zip gradle-samples-0.6.1



