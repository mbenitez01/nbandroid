package org.nbandroid.netbeans.gradle;

import java.io.InputStream;

/**
 *
 * @author radim
 */
public class GradleTests {

  public static final String TEST_ARCHIVE_NAME = "gradle-samples-0.14.4.zip";
  public static final String TEST_ARCHIVE_DIR = "gradle-samples-0.14.4";
  
  public static InputStream testArchiveStream() {
    return GradleTests.class.getResourceAsStream(TEST_ARCHIVE_NAME);
  }
}
