package org.nbandroid.netbeans.gradle;

import com.google.common.collect.Iterables;
import java.io.File;
import java.net.URL;
import java.util.Arrays;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.project.Project;
import org.netbeans.modules.android.core.sdk.DalvikPlatform;
import org.netbeans.modules.android.project.spi.DalvikPlatformResolver;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

import org.netbeans.api.java.project.JavaProjectConstants;
import org.netbeans.api.java.queries.SourceForBinaryQuery;
import org.netbeans.api.project.ProjectUtils;
import org.netbeans.api.project.SourceGroup;
import org.netbeans.api.project.Sources;
import org.netbeans.gradle.project.api.task.BuiltInGradleCommandQuery;
import org.netbeans.junit.MockServices;
import org.netbeans.modules.android.project.api.AndroidConstants;
import org.netbeans.modules.android.project.api.AndroidManifestSource;
import org.netbeans.modules.android.project.api.AndroidProjects;
import org.netbeans.spi.java.queries.SourceForBinaryQueryImplementation;

import static org.nbandroid.netbeans.gradle.GradleProjectFixture.*;

public class TicTacToeGradleProjectTest {
  private static GradleProjectFixture prjFix;
  private static Project prj;
  private static Project lib;
  private static FileObject foProjectSrc;
  private static PluginsFixture pluginsFix;

  @BeforeClass
  public static void setUpClass() throws Exception {
    MockServices.setServices();
    pluginsFix = new PluginsFixture().setupSDK().setupGradle();
    prjFix = new GradleProjectFixture.Builder()
        .usePrefix(GradleTests.TEST_ARCHIVE_DIR)
        .useName("tictactoe")
        .useIs(GradleTests.testArchiveStream())
        .create();

    prj = prjFix.loadProject("app");
    lib = prjFix.loadProject("lib");
    
    foProjectSrc = prj.getProjectDirectory().getFileObject("src/main/java");
  }

  @AfterClass
  public static void clear() {
    prjFix.tearDown();
  }

  @Test
  public void isAndroidProject() throws Exception {
    assertTrue(AndroidProjects.isAndroidProject(prj));
    assertTrue(AndroidProjects.isAndroidProject(lib));
  }
  
  @Test
  public void basic() throws Exception {
    // get the classpath
    verifyClasspath(prj, foProjectSrc, ClassPath.SOURCE, 
        "tictactoe/app/src/main/java" 
        // "tictactoe/app/build/source/r/debug" - will be added after build
        );
    verifyClasspath(prj, foProjectSrc, ClassPath.COMPILE, 
        "tictactoe/app/build/intermediates/exploded-aar/tictactoe/lib/unspecified/classes.jar"
        );
    verifyClasspath(prj, foProjectSrc, ClassPath.BOOT, "android.jar");
    
    DalvikPlatformResolver platformProvider = prj.getLookup().lookup(DalvikPlatformResolver.class);
    assertNotNull(platformProvider);
    DalvikPlatform platform = platformProvider.findDalvikPlatform();
    assertNotNull(platform);
  }
  
//  @Test
  public void sourceForAndroidLibraryDependency() {
    URL libRoot = FileUtil.urlForArchiveOrDir(new File(prjFix.prjDir, "build/intermediates/exploded-aar/tictactoe/lib/unspecified/classes.jar"));
    SourceForBinaryQueryImplementation sfb = prj.getLookup().lookup(SourceForBinaryQueryImplementation.class);
    SourceForBinaryQuery.Result libSources = sfb.findSourceRoots(libRoot);
    assertNotNull(libSources);
    // check there is main source root from library
    assertTrue(Arrays.toString(libSources.getRoots()),
        Arrays.asList(libSources.getRoots()).contains(lib.getProjectDirectory().getFileObject("src/main/java")));
  }
  
  @Test
  public void sources() throws Exception {
    Sources sources = ProjectUtils.getSources(prj);
    assertNotNull(sources);

    final FileObject foSrc = prj.getProjectDirectory().getFileObject("src/main/java/com/example/android/tictactoe/MainActivity.java");
    SourceGroup[] sourceGroups = sources.getSourceGroups(JavaProjectConstants.SOURCES_TYPE_JAVA);
    assertTrue("java source in " + sources,
        Iterables.any(
            Arrays.asList(sourceGroups), 
            sourceGroupContainsFile(foSrc)));
    final FileObject foRes = prj.getProjectDirectory().getFileObject("src/main/res/layout/main.xml");
    SourceGroup[] sourceGroups2 = sources.getSourceGroups(AndroidConstants.SOURCES_TYPE_ANDROID_RES);
    assertTrue("app resource in " + sources,
        Iterables.any(
            Arrays.asList(sourceGroups2), 
            sourceGroupContainsFile(foRes)));
  }
  
  @Test
  public void manifest() throws Exception {
    AndroidManifestSource ams = prj.getLookup().lookup(AndroidManifestSource.class);
    assertNotNull(ams);

    FileObject foSrc = prj.getProjectDirectory().getFileObject("src/main/" + AndroidConstants.ANDROID_MANIFEST_XML);
    assertEquals(foSrc, ams.get());
  }
  
  @Test
  public void commands() throws Exception {
    BuiltInGradleCommandQuery commands = prj.getLookup().lookup(BuiltInGradleCommandQuery.class);
    assertNotNull(commands);
  }
}