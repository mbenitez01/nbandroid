package org.nbandroid.netbeans.gradle;

import com.google.common.collect.Iterables;
import java.util.Arrays;
import java.util.logging.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nbandroid.netbeans.gradle.config.BuildVariant;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.project.Project;
import org.netbeans.modules.android.core.sdk.DalvikPlatform;
import org.netbeans.modules.android.core.sdk.DalvikPlatformManager;
import org.netbeans.modules.android.project.spi.DalvikPlatformResolver;
import org.openide.filesystems.FileObject;
import org.netbeans.api.java.project.JavaProjectConstants;
import org.netbeans.api.project.ProjectUtils;
import org.netbeans.api.project.SourceGroup;
import org.netbeans.api.project.Sources;
import org.netbeans.junit.MockServices;
import org.netbeans.modules.android.project.api.AndroidProjects;

import static org.junit.Assert.*;
import static org.nbandroid.netbeans.gradle.GradleProjectFixture.*;

public class FlavorsGradleProjectTest {
  private static final Logger LOG = Logger.getLogger(DependenciesGradleProjectTest.class.getName());
  
  private static PluginsFixture pluginsFix;
  private static GradleProjectFixture prjFix;
  private static FileObject foProjectSrc;

  public static class BuildVariantInitializer implements AndroidGradleCallback {

    @Override
    public void customize(AndroidGradleExtension ext, Project prj) {
      BuildVariant bv = Iterables.getOnlyElement(Iterables.filter(ext.getItems(), BuildVariant.class));
      bv.setVariantName("f2FaDebug");
    }
  }
  
  @BeforeClass
  public static void setUpClass() throws Exception {
    MockServices.setServices(BuildVariantInitializer.class);
    pluginsFix = new PluginsFixture().setupSDK().setupGradle();
    prjFix = new GradleProjectFixture.Builder()
        .usePrefix(GradleTests.TEST_ARCHIVE_DIR)
        .useName("flavors")
        .useIs(GradleTests.testArchiveStream())
        .create();
    
    foProjectSrc = prjDir(prjFix).getFileObject("src/main/java");
  }

  @AfterClass
  public static void clear() {
    prjFix.tearDown();
  }

  @Test
  public void isAndroidProject() throws Exception {
    assertTrue(AndroidProjects.isAndroidProject(prjFix.prj));
  }
  
  @Test
  public void basic() throws Exception {
    BuildVariant bv = prjLookup(prjFix).lookup(BuildVariant.class);
    bv.setVariantName("f2FaRelease");
    // get the classpath
    verifyClasspath(prjFix, foProjectSrc, ClassPath.SOURCE, 
        "flavors/src/main/java",
        "flavors/src/fa/java",
        "flavors/src/f2/java"
        );
    
    DalvikPlatformResolver platformProvider = prjLookup(prjFix).lookup(DalvikPlatformResolver.class);
    assertNotNull(platformProvider);
    DalvikPlatform platform = platformProvider.findDalvikPlatform();
    assertNotNull(platform);
    
    bv.setVariantName("f1FaRelease");
    BuildVariant.RP.post(new Runnable() { @Override public void run() {}}).waitFinished();
    verifyClasspath(prjFix, foProjectSrc, ClassPath.SOURCE, 
        "flavors/src/main/java",
        "flavors/src/fa/java",
        "flavors/src/f1/java"
        );
    bv.setVariantName("f2FaDebug");
    BuildVariant.RP.post(new Runnable() { @Override public void run() {}}).waitFinished();
  }
  
  @Test
  public void sources() throws Exception {
    Sources sources = ProjectUtils.getSources(prjFix.prj);
    assertNotNull(sources);

    final FileObject foSrc = prjDir(prjFix).getFileObject("src/main/java/com/android/tests/flavors/MainActivity.java");
    SourceGroup[] sourceGroups = sources.getSourceGroups(JavaProjectConstants.SOURCES_TYPE_JAVA);
    assertTrue("java source in " + sources,
        Iterables.any(
            Arrays.asList(sourceGroups), 
            sourceGroupContainsFile(foSrc)));
    
    BuildVariant bv = prjLookup(prjFix).lookup(BuildVariant.class);
    assertEquals("f2FaDebug", bv.getVariantName());
    final FileObject flavorSrc = 
        prjDir(prjFix).getFileObject("src/fa/java/com/android/tests/flavors/group2/SomeClass.java");
    assertTrue("java source in " + sources,
        Iterables.any(
            Arrays.asList(sourceGroups), 
            sourceGroupContainsFile(flavorSrc)));
  }
  @Test
  public void testSourcePath() throws Exception {
    BuildVariant bv = prjLookup(prjFix).lookup(BuildVariant.class);
    bv.setVariantName("f1FaRelease");
    BuildVariant.RP.post(new Runnable() { @Override public void run() {}}).waitFinished();
    // get the classpath
    verifyClasspath(prjFix, prjDir(prjFix).getFileObject("src/androidTest/java"), ClassPath.SOURCE, 
        "flavors/src/androidTest/java",
        "flavors/src/androidTestF1/java",
        "flavors/src/androidTestFa/java");
    bv.setVariantName("f2FaDebug");
    BuildVariant.RP.post(new Runnable() { @Override public void run() {}}).waitFinished();
  }
}