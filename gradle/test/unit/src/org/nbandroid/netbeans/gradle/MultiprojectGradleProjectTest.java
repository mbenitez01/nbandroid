package org.nbandroid.netbeans.gradle;

import java.io.File;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.project.Project;
import org.netbeans.modules.android.core.sdk.DalvikPlatform;
import org.netbeans.modules.android.project.spi.DalvikPlatformResolver;
import org.openide.filesystems.FileObject;
import org.netbeans.junit.MockServices;
import org.netbeans.modules.android.project.api.AndroidProjects;

import static org.junit.Assert.*;
import static org.nbandroid.netbeans.gradle.GradleProjectFixture.*;
import org.netbeans.spi.java.classpath.ClassPathProvider;
import org.openide.filesystems.FileUtil;

public class MultiprojectGradleProjectTest {

  private static GradleProjectFixture prjFix;
  private static Project app;
  private static Project lib;
  private static Project util;
  private static FileObject foLibrarySrc;
  private static PluginsFixture pluginsFix;

  @BeforeClass
  public static void setUpClass() throws Exception {
    MockServices.setServices();
    pluginsFix = new PluginsFixture().setupSDK().setupGradle();
    prjFix = new GradleProjectFixture.Builder()
        .usePrefix(GradleTests.TEST_ARCHIVE_DIR)
        .useName("multiproject")
        .useIs(GradleTests.testArchiveStream())
        .create();
    app = prjFix.loadProject("app");
    lib = prjFix.loadProject("library");
    util = prjFix.loadProject("util");
    
    foLibrarySrc = lib.getProjectDirectory().getFileObject("src/main/java");
  }

  @AfterClass
  public static void clear() {
    prjFix.tearDown();
  }

  @Test
  public void isAndroidProject() throws Exception {
    assertTrue(AndroidProjects.isAndroidProject(app));
    assertTrue(AndroidProjects.isAndroidProject(lib));
    assertFalse(AndroidProjects.isAndroidProject(prjFix.prj));
    assertFalse(AndroidProjects.isAndroidProject(util));
  }
  
  @Test
  public void classpathDependencyFromJavaProject() throws Exception {
    // Build and wait (will build dependencies first)
    prjFix.buildAction()
        .andWaitForFile(new File(FileUtil.toFile(prjDir(prjFix)), "library/build/generated/source/buildConfig/debug"))
        .invoke();
    // get the classpath
    verifyClasspath(lib, foLibrarySrc, ClassPath.COMPILE, 
        "util/build/classes/main", // or "util/build/libs-1.0.jar"
        "guava-11.0.2.jar");
  }
    
  @Test
  public void basic() throws Exception {
    DalvikPlatformResolver platformProvider = app.getLookup().lookup(DalvikPlatformResolver.class);
    assertNotNull(platformProvider);
    DalvikPlatform platform = platformProvider.findDalvikPlatform();
    assertNotNull(platform);
    
    final ClassPathProvider cpp = util.getLookup().lookup(ClassPathProvider.class);
    assertNotNull(cpp);
  }
}