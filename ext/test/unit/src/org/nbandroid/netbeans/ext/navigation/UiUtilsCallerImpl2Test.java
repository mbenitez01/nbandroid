package org.nbandroid.netbeans.ext.navigation;

import java.io.File;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.netbeans.modules.android.core.sdk.DalvikPlatform;
import org.netbeans.modules.android.core.sdk.DalvikPlatformManager;
import org.netbeans.modules.android.project.AndroidProject;
import org.netbeans.modules.android.project.AndroidTestFixture;
import org.openide.filesystems.FileObject;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import org.netbeans.modules.android.project.FileUtilities;
import org.netbeans.modules.android.project.api.ResourceRef;
import org.netbeans.modules.android.project.spi.AndroidProjectDirectory;

/**
 *
 * @author radim
 */
public class UiUtilsCallerImpl2Test {
  
  private static AndroidTestFixture fixture;
  private static FileObject skeletonPrjDir;
  private static AndroidProject skeletonPrj;
  private static FileObject someSource1;
  private static FileObject skeletonLayout;
  private static FileObject someLayout1;
  private static DalvikPlatform testedPlatform;

  @BeforeClass
  public static void setUpClass() throws Exception {
    fixture = AndroidTestFixture.create().withProject("Snake", "samples/android-8/Snake")
        .withProject("SkeletonApp", "samples/android-8/SkeletonApp");

    skeletonPrjDir = fixture.getProjectFolder("SkeletonApp");
    skeletonPrj = (AndroidProject) fixture.getProject("SkeletonApp");
    skeletonLayout = skeletonPrjDir.getFileObject("res/layout/skeleton_activity.xml");
    testedPlatform = DalvikPlatformManager.getDefault().findPlatformForTarget("android-8");
    someSource1 = skeletonPrjDir.getFileObject("src/com/example/android/snake/SkeletonActivity.java");
    
    FileUtilities.recursiveDelete(new File(
        skeletonPrj.getLookup().lookup(AndroidProjectDirectory.class).get(), "res"));
    skeletonPrjDir.refresh();
  }

  @AfterClass
  public static void delete() {
    fixture.tearDown();
  }
  
  @Test
  public void missingLayoutFolder() {
    UiUtilsCallerImpl.OpenExecutor open = mock(UiUtilsCallerImpl.OpenExecutor.class);
    UiUtilsCallerImpl ui = new UiUtilsCallerImpl(open);
    assertFalse(ui.open(
        skeletonPrjDir.getFileObject("src/com/example/android/skeletonapp/SkeletonActivity.java"), 
        new ResourceRef(true, "com.example.android.skeletonapp", "id", "editor", null)));
    verifyZeroInteractions(open);
    assertFalse(ui.open(someSource1, new ResourceRef(true, "com.example.android.skeletonapp", "layout", "snake_layout", null)));
    verifyZeroInteractions(open);
  }
}