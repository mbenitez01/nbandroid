package org.nbandroid.netbeans.ext.navigation;

import javax.lang.model.element.Element;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.modules.android.project.api.ResourceRef;
import org.openide.filesystems.FileObject;

/**
 *
 * @author radim
 */
public interface UiUtilsCaller {

  boolean open(FileObject fo, ResourceRef resRef);
  boolean open(final ClasspathInfo cpInfo, final ElementHandle<? extends Element> el);

  void beep();

  void warnCannotOpen(String displayName);
    
}
