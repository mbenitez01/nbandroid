package org.netbeans.modules.android.project;

import org.junit.Test;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectManager;
import org.openide.filesystems.FileUtil;

import java.io.File;

import static org.junit.Assert.*;

public class AndroidProjectFactoryTest {

  @Test
  public void findProject() throws Exception {
    File tempFolder = File.createTempFile("junit", "");
    tempFolder.delete();
    tempFolder.mkdir();
    try {
      TestUtils.unzip(AndroidProjectFactoryTest.class.getResourceAsStream("builtApp.zip"), tempFolder);
      File prjDir = new File(tempFolder,  "AndroidApplication1");
      Project prj = ProjectManager.getDefault().findProject(FileUtil.toFileObject(prjDir));
      assertNotNull(prj);
      Project prjBin = ProjectManager.getDefault().findProject(FileUtil.toFileObject(new File(prjDir, "bin")));
      assertNull(prjBin);
    } finally {
      FileUtilities.recursiveDelete(tempFolder);
    }
  }

  @Test
  public void ignoreMavenProjectSubfolder() throws Exception {
    File tempFolder = File.createTempFile("junit", "");
    tempFolder.delete();
    tempFolder.mkdir();
    try {
      TestUtils.unzip(AndroidProjectFactoryTest.class.getResourceAsStream("mvnandroid-example.zip"), tempFolder);
      File prjDir = new File(tempFolder,  "mvnandroid-example");
      Project prj = ProjectManager.getDefault().findProject(FileUtil.toFileObject(prjDir));
      // this would require Maven project on classpath
      // assertNotNull(prj);
      Project prjBin = ProjectManager.getDefault().findProject(FileUtil.toFileObject(new File(prjDir, "src/main")));
      assertNull(prjBin);
    } finally {
      FileUtilities.recursiveDelete(tempFolder);
    }
  }
}
