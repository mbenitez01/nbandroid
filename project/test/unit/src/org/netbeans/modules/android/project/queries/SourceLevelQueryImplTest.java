/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.netbeans.modules.android.project.queries;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import org.netbeans.modules.android.project.*;
import java.nio.charset.Charset;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.netbeans.api.project.ProjectManager;
import org.openide.filesystems.FileObject;
import static org.junit.Assert.*;
import org.openide.filesystems.FileUtil;

public class SourceLevelQueryImplTest {

  private static AndroidTestFixture fixture;

  private static FileObject projdir;
  private static FileObject someSource1;

  @BeforeClass
  public static void setUpClass() throws Exception {
    fixture = AndroidTestFixture.create().withProject("Snake", "samples/android-15/Snake");
    projdir = fixture.getProjectFolder("Snake");

    someSource1 = projdir.getFileObject("src/com/example/android/snake/Snake.java");
  }


  @AfterClass
  public static void delete() {
    fixture.tearDown();
  }
  
  @Test
  public void fileEncoding() throws Exception {
    FileObject propertiesFile = projdir.createData("ant.properties");
    Files.write("java.source=1.6\n", FileUtil.toFile(propertiesFile), Charsets.UTF_8);
    propertiesFile.refresh();
    final AndroidProjectImpl proj = (AndroidProjectImpl) ProjectManager.getDefault().findProject(projdir);
    assertNotNull(someSource1);
    
    // create again with fresh properties evaluator
    final SourceLevelQueryImpl slq = new SourceLevelQueryImpl(new PropertiesHelper(proj).evaluator());
    String sourceLevel = slq.getSourceLevel(someSource1);
    assertEquals("1.6", sourceLevel);
  }

}
