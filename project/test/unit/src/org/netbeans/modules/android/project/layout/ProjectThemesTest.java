package org.netbeans.modules.android.project.layout;

import com.google.common.collect.Lists;
import java.util.List;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.netbeans.modules.android.project.AndroidTestFixture;

/**
 *
 * @author radim
 */
public class ProjectThemesTest {
  
  private static AndroidTestFixture fixture;

  @BeforeClass
  public static void setUpClass() throws Exception {
    fixture = AndroidTestFixture.create().withProject("ApiDemos15", "samples/android-15/ApiDemos").
        withProject("Compat", "samples/android-15/ActionBarCompat");
  }
  
  @AfterClass
  public static void tearDownClass() {
    fixture.tearDown();
  }
  
  @Test
  public void testGetProjectThemesNull() {
    ProjectThemes instance = new ProjectThemes(null);
    List<ThemeData> themes = Lists.newArrayList(instance.getProjectThemes());
    assertTrue(themes.isEmpty());
  }
  
  @Test
  public void testGetProjectThemes() {
    ProjectThemes instance = new ProjectThemes(fixture.getProject("ApiDemos15"));
    List<ThemeData> themes = Lists.newArrayList(instance.getProjectThemes());
    assertTrue(themes.toString(), themes.contains(new ThemeData("ThemeHoloDialog", true)));
  }
  
  @Test
  public void testGetProjectThemesConfigured() {
    // check that we get themes defined in styles.xml in qualified values-<something> folder
    ProjectThemes instance = new ProjectThemes(fixture.getProject("Compat"));
    List<ThemeData> themes = Lists.newArrayList(instance.getProjectThemes());
    assertTrue(themes.toString(), themes.contains(new ThemeData("ActionBarTitle", true)));
  }
}
