package org.netbeans.modules.android.project.layout;

import com.android.ide.common.rendering.api.RenderSession;
import com.android.ide.common.rendering.api.Result;
import com.android.ide.common.rendering.api.ViewInfo;
import com.android.ide.common.resources.configuration.FolderConfiguration;
import com.android.ide.common.resources.configuration.VersionQualifier;
import com.android.resources.Density;
import com.android.resources.Keyboard;
import com.android.resources.KeyboardState;
import com.android.resources.Navigation;
import com.android.resources.NavigationState;
import com.android.resources.ScreenOrientation;
import com.android.resources.ScreenRatio;
import com.android.resources.ScreenSize;
import com.android.resources.TouchScreen;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Before;
import org.nbandroid.netbeans.test.AbstractAndroidPlatformTest;
import org.nbandroid.netbeans.test.PlatformVersions;
import org.netbeans.modules.android.core.sdk.DalvikPlatform;
import org.netbeans.modules.android.core.sdk.DalvikPlatformManager;
import org.netbeans.modules.android.project.AndroidProject;
import org.netbeans.modules.android.project.AndroidTestFixture;
import org.netbeans.modules.android.project.api.AndroidClassPath;
import org.netbeans.modules.parsing.impl.indexing.CacheFolder;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

import static org.junit.Assert.*;
import org.junit.Assume;

// TODO start with android-8?
@PlatformVersions(type = PlatformVersions.PlatformType.PURE, minimumVersion = 12)
public class RenderingServiceCrossVersionTest extends AbstractAndroidPlatformTest {
  
  private static final String ANT_DIR = System.getProperty("test.all.ant.home");
  private static AndroidTestFixture fixture;

  private static FileObject skeletonPrjDir;
  private static AndroidProject skeletonPrj;
  private static FileObject snakePrjDir;
  private static AndroidProject snakePrj;
  private static FileObject someSource1;
  private static FileObject skeletonLayout;
  private static FileObject someLayout1;

  @BeforeClass
  public static void setUpClass() throws Exception {
    File cacheFolder = File.createTempFile("cache", "");
    CacheFolder.setCacheFolder(FileUtil.toFileObject(cacheFolder));
    fixture = AndroidTestFixture.create().withProject("Snake", "samples/android-8/Snake")
        .withProject("SkeletonApp", "samples/android-8/SkeletonApp");
    
    snakePrjDir = fixture.getProjectFolder("Snake");
    snakePrj = (AndroidProject) fixture.getProject("Snake");
    someSource1 = snakePrjDir.getFileObject("src/com/example/android/snake/Snake.java");
    someLayout1 = snakePrjDir.getFileObject("res/layout/snake_layout.xml");

    skeletonPrjDir = fixture.getProjectFolder("SkeletonApp");
    skeletonPrj = (AndroidProject) fixture.getProject("SkeletonApp");
    skeletonLayout = skeletonPrjDir.getFileObject("res/layout/skeleton_activity.xml");
  }

  @AfterClass
  public static void delete() {
    fixture.tearDown();
  }
  
  private DalvikPlatform testedPlatform;

  @Before
  public void setUp() {
    testedPlatform = DalvikPlatformManager.getDefault().findPlatformForTarget(getPlatform().target);
  }
  
  private FolderConfiguration createTestConfiguration(DalvikPlatform platform) throws Exception {
    FolderConfiguration folderCfg = RenderingServiceTest.createConfig(
        1280, 800, // size 1 and 2. order doesn't matter.
        // Orientation will drive which is w and h
        ScreenSize.XLARGE,
        ScreenRatio.LONG,
        ScreenOrientation.LANDSCAPE,
        Density.MEDIUM,
        TouchScreen.FINGER,
        KeyboardState.SOFT,
        Keyboard.QWERTY,
        NavigationState.EXPOSED,
        Navigation.NONAV,
        platform.getAndroidTarget().getVersion().getApiLevel());
    folderCfg.setVersionQualifier(new VersionQualifier(platform.getAndroidTarget().getVersion().getApiLevel()));
    return folderCfg;
  }

  @Test
  public void createRenderSession() throws Exception {
    RenderingService service = RenderServiceFactory.createService(snakePrj, 
        createTestConfiguration(testedPlatform), testedPlatform);
    Reader layoutReader = new InputStreamReader(someLayout1.getInputStream());
    String themeName = "Theme";
    boolean isProjectTheme = false;
    
    RenderSession result = service.createRenderSession(null, snakePrj.getLookup().lookup(AndroidClassPath.class),
        layoutReader, themeName, isProjectTheme, "snake_layout");
    assertNotNull(result);
  }

  @Test
  public void renderImageSimpleView() throws Exception {
    Assume.assumeTrue(getPlatform().apiLevel != 20); // ignore wearables
    RenderingService service = RenderServiceFactory.createService(skeletonPrj, 
        createTestConfiguration(testedPlatform), testedPlatform);
    Reader layoutReader = new InputStreamReader(skeletonLayout.getInputStream());
    String themeName = "Theme";
    boolean isProjectTheme = false;
    
    RenderSession session = service.createRenderSession(
        null, fixture.getProject("SkeletonApp").getLookup().lookup(AndroidClassPath.class),
        layoutReader, themeName, isProjectTheme, "skeleton_activity");
    
    // get the status of the render
    Result result = session.getResult();
    if (!result.isSuccess()) {
      fail(result.getErrorMessage());
    }
    BufferedImage img = session.getImage();
    assertNotNull(img);
//    File out = File.createTempFile("nbandroid-skeleton", "png");
//    ImageIO.write(img, "png", out);
//    System.err.println("Output stored into " + out.getPath());

    // read the views
    displayViewObjects(session.getRootViews());
  }
  
  @Test
  @PlatformVersions(minimumVersion = 18)
  public void renderWithHoloTheme() throws Exception {
    Assume.assumeTrue(getPlatform().apiLevel != 20); // ignore wearables
    RenderingService service = RenderServiceFactory.createService(skeletonPrj, 
        createTestConfiguration(testedPlatform), testedPlatform);
    Reader layoutReader = new InputStreamReader(skeletonLayout.getInputStream());
    String themeName = "Theme.Holo";
    boolean isProjectTheme = false;
    
    RenderSession session = service.createRenderSession(
        null, fixture.getProject("SkeletonApp").getLookup().lookup(AndroidClassPath.class),
        layoutReader, themeName, isProjectTheme, "skeleton_activity");
    
    // get the status of the render
    Result result = session.getResult();
    if (!result.isSuccess()) {
      fail(result.getErrorMessage());
    }
    BufferedImage img = session.getImage();
    assertNotNull(img);
    // read the views
    displayViewObjects(session.getRootViews());
  }

  private static void displayViewObjects(List<ViewInfo> rootViews) {
    for (ViewInfo info : rootViews) {
      displayView(info, "");
    }
  }

  private static void displayView(ViewInfo info, String indent) {
    // display info data
    System.out.println(indent + info.getClassName()
        + " [" + info.getLeft() + ", " + info.getTop() + ", "
        + info.getRight() + ", " + info.getBottom() + "]");

    // display the children
    List<ViewInfo> children = info.getChildren();
    if (children != null) {
      indent += "\t";
      for (ViewInfo child : children) {
        displayView(child, indent);
      }
    }
  }
}
